<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LTuristicos */

$this->title = 'Create L Turisticos';
$this->params['breadcrumbs'][] = ['label' => 'L Turisticos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lturisticos-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
