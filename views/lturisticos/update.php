<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LTuristicos */

$this->title = 'Update L Turisticos: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'L Turisticos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lturisticos-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
