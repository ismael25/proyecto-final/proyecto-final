<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'L Turisticos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lturisticos-index">

    

    <p>
        <?= Html::a('Create L Turisticos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod',
            'nombre',
            'dirección',
            'ciudad',
            'descripción',
            //'cod_destino',
            //'coste',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
