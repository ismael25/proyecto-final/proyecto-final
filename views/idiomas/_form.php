<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="idiomas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_destino')->textInput() ?>

    <?= $form->field($model, 'idiomas')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
