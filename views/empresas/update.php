<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Empresas */

$this->title = 'Update Empresas: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="empresas-update">

    

    <?= $this->render('_form', [
        'model' => $model,
        'model_destinos' => $model_destinos,
    ]) ?>

</div>
