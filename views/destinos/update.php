<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destinos */

$this->title = 'Update Destinos: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Destinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="destinos-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
