<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destinos */

$this->title = 'Create Destinos';
$this->params['breadcrumbs'][] = ['label' => 'Destinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destinos-create">

 

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
