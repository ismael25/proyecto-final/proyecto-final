<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pisos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pisos-index">

    

    <p>
        <?= Html::a('Create Pisos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod',
            'dirección',
            'servicios',
            'tipo',
            'ciudad',
            //'coste',
            //'cod_destino',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
