<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pisos */

$this->title = 'Update Pisos: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Pisos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pisos-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
