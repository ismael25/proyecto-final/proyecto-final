<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pisos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pisos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dirección')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servicios')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coste')->textInput() ?>

    <?= $form->field($model, 'cod_destino')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
