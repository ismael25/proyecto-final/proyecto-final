<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Descargans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="descargan-index">



    <p>
        <?= Html::a('Create Descargan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod',
            'cod_estudiantes',
            'cod_apps',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
