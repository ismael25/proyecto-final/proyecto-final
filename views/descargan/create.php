<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Descargan */

$this->title = 'Create Descargan';
$this->params['breadcrumbs'][] = ['label' => 'Descargans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="descargan-create">

  

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
