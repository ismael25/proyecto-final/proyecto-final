<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Descargan */

$this->title = 'Update Descargan: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Descargans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="descargan-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
