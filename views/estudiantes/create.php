<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estudiantes */

$this->title = 'Create Estudiantes';
$this->params['breadcrumbs'][] = ['label' => 'Estudiantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudiantes-create">

   

    <?= $this->render('_form', [
        'model' => $model,
        'model_destinos' => $model_destinos,
        'model_empresas' => $model_empresas,
        'model_pisos' => $model_pisos,
    ]) ?>

</div>
