<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Estudiantes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estudiantes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estudios')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tiempoEstancia')->textInput() ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>

     <?php $listadestino =ArrayHelper::map($model_destinos ,'cod' , 'nombre');?>
    
    <?= $form->field($model, 'cod_destino')->dropDownList($listadestino)->label('Destino') ?>
    
    <?php $listapisos =ArrayHelper::map($model_pisos ,'cod' , 'dirección');?>
    <?= $form->field($model, 'cod_piso') ->dropDownList($listapisos)->label('Piso')?>

     <?php $listaEmpresa =ArrayHelper::map($model_empresas ,'cod' , 'nombre');?>
    <?= $form->field($model, 'cod_empresa')->dropDownList($listaEmpresa)->label('Empresa') ?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
