<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Estudiantes */

$this->title = 'Update Estudiantes: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Estudiantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estudiantes-update">

    

    <?= $this->render('_form', [
        'model' => $model,
        'model_destinos' => $model_destinos,
        'model_empresas' => $model_empresas,
        'model_pisos' => $model_pisos,
    ]) ?>

</div>
