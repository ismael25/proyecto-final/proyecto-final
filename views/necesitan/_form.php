<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Necesitan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="necesitan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_estudiantes')->textInput() ?>

    <?= $form->field($model, 'cod_documentos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
