<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Necesitan */

$this->title = 'Create Necesitan';
$this->params['breadcrumbs'][] = ['label' => 'Necesitans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="necesitan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
