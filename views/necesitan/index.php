<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Necesitans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="necesitan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Necesitan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod',
            'cod_estudiantes',
            'cod_documentos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
