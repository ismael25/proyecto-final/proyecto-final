<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$id = $model->cod;
?>
<div class="datos ">
    <h3><?= $model->nombre ?></h3>
    <div class="col-md-6">
        <?= Html::img("@$model->bandera", ['alt' => 'Bandera' , 'class' => 'image']) ?>
    </div>
    <div class="col-md-6">
        <h5><b>Coste Gestion: </b></h5>
        <p><?= $model->coste ?> €</p>
        <h5><b>Idiomas: </b></h5>
        <?= implode('<p></p>', ArrayHelper::getColumn($model->idiomas, 'idiomas')) ?>
       
    </div>
     <?= Html::a('Lugares Turisticos', ['site/mlturisticos' , 'id' => $id], ['class' => 'btn btn-info'])?> 
</div>
