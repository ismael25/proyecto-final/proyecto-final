<?php

use\yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
$titulo="Lugares Turisticos"
?>

<?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => "_lturisticos",
        'layout' => '{items}',
        ]);
?>
