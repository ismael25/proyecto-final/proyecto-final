<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
$imagen = "@web/imagenes/travel-map.png"; 
?>
<div class="datos row ">
<div>
    <h3><?= $model->nombre ?></h3>
    <div class="col-md-6">
    <?=
          Html::img($imagen, ['alt' => 'casa' , 'class' => 'image2']);
    ?>
    </div>
    <div class="col-md-6">
        <p><b>Ciudad:</b> <?= $model->ciudad ?></p>
        <p><b>Dirección:</b> <?= $model->dirección ?></p>
        <p><b>Pais:</b> <?= $model->codDestino->nombre ?></p>
        <p><b>Coste:</b> <?= $model->coste ?> €</p>
    </div>
    <p><?= $model->descripción ?></p>
    
</div>
</div>