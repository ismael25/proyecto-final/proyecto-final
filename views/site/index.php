<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Erasmus Easy';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>¡Bienvenido a ErasmusEasy!</h2>

        <p class="lead">En nuestra pagina web podra organizar su viaje erasmus de manera sencila y adaptandose
        a sus necesidades lo mejor posible ademas de recibir el presupuesto de maner inmediata .</p>

        <p></p>
    </div>
    <!-- Carousel container -->
    <div id="slider" class="carousel slide" data-ride="carousel" style="width:1000px;height:500px;margin:auto;">

        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#slider" data-slide-to="0" class="active"></li>
            <li data-target="#sliders" data-slide-to="1"></li>
            <li data-target="#slider" data-slide-to="2"></li>
            <li data-target="#slider" data-slide-to="3"></li>
            <li data-target="#slider" data-slide-to="4"></li>
        </ol>

        <!-- Content -->
        <div class="carousel-inner" role="listbox">

            <!-- Slide 1 -->
            <div class="item active">
                <?= Html::img('@web/imagenes/Europa.jpg', ['alt' => 'First slide']) ?>
                <div class="carousel-caption">
                    <div class="letras">
                        <h3><b>Destinos</b></h3>
                    <p>Tenemos una selección de paises donde podras formarte tanto laboralamente como crecer en lo personal</p>
                     </div>
                    <?= Html::a('Destinos', ['site/mdestinos'], ['class' => 'btn btn-success'])?>
                </div>
            </div>

            <!-- Slide 2 -->
            <div class="item">
                <?= Html::img('@web/imagenes/empresa.jpg', ['alt' => 'Second slide']) ?>
                <div class="carousel-caption">
                    <div class="letras">
                    <h3><b>Empresas</b></h3>
                    <p>Poseemos un gran catalogo de empresas donde podra usted realizar sus practicas </p><br>
                    </div>
                    <?= Html::a('Empresas', ['site/mempresas'], ['class' => 'btn btn-success'])?>
                </div> 
            </div>

            <!-- Slide 3 -->
            <div class="item">
                <?= Html::img('@web/imagenes/viviendas.jpg', ['alt' => 'Third slide']) ?>
                <div class="carousel-caption">
                    <div class="letras">
                    <h3><b>Viviendas</b></h3>
                    <p>Aquí podra encontrar numerossos pisos donde alojarse</p><br>  
                    </div>
                    <?= Html::a('Viviendas', ['site/mpisos'], ['class' => 'btn btn-success'])?>
                </div> 
            </div>

            <!-- Slide 4 -->
            <div class="item">
               <?= Html::img('@web/imagenes/documentos.jpg', ['alt' => 'Forth slide']) ?>
               <div class="carousel-caption">
               <div class="letras">
                   <h3><b>Documentos</b></h3>
                <p>Ademas le recordamos aqui todo los documentos que podra necesitar durante su estancia o para el desplazamiento</p><br>
                </div>
                <?= Html::a('Documentos', ['site/mdocumentos'], ['class' => 'btn btn-success'])?>    
            </div>
        </div>

        <!-- Slide 5 -->
        <div class="item">
         <?= Html::img('@web/imagenes/apps.jpg', ['alt' => 'Fifth slide']) ?>
         <div class="carousel-caption">
            <div class="letras">
            <h3><b>Apps</b></h3>
            <p>Tambien le recomendamos una serie de apps que le podran ser de utilida para su viaje</p><br>
            </div>
            <?= Html::a('APPS', ['site/mapps'], ['class' => 'btn btn-success'])?>
        </div>  
    </div>

</div>

<!-- Previous/Next controls -->
<a class="left carousel-control" href="#slider" role="button" data-slide="prev">
    <span class="icon-prev" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#slider" role="button" data-slide="next">
    <span class="icon-next" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
</div>
</div>
