<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
$imagen = "@web/imagenes/google-docs.png"; 
?>
<div class="datos row ">
<div>
    <h3><?= $model->nombre ?></h3>
    <div class="col-md-2">
    </div>
    <div class="col-md-4">
    <?=
          Html::img($imagen, ['alt' => 'docs' , 'class' => 'image2']);
    ?>
    </div>
    <div class="col-md-5">
    <h5><b>Utilidad: </b></h5>
    <p><?= $model->utilidad ?></p>
    <h5><b>Lugar obtencion: </b></h5>
    <p><?= $model->lugar_obtencion ?></p>
    </div>
</div>
</div>

