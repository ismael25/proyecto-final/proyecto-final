<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
if($model->categoría == 'Informatica'){
        $imagen= "@web/imagenes/ordenador.png"; 
    }elseif($model->categoría == 'Economia'){
        $imagen= "@web/imagenes/dolar.png"; 
    }else{
        $imagen= "@web/imagenes/balon.png"; 
    }
?>
<div class="datos row ">
<div>
    <h3><?= $model->nombre ?></h3>
     <div class="col-md-6">
    <?=
          Html::img($imagen, ['alt' => 'casa' , 'class' => 'image2']);
    ?>
    <h5><b>Categoria: </b></h5>
    <p><?= $model->categoría ?></p>
    <h5><b>Ciudad: </b></h5>
    <p><?= $model->ciudad ?></p>
    </div>
    <div class="col-md-6">
    <h5><b>Dirección: </b></h5>
    <p><?= $model->dirección ?></p>
    <h5><b>Trabajadores: </b></h5>
    <p><?= $model->num_trabajadores ?></p>
    <h5><b>Pais: </b></h5>
    <p><?= $model->codDestino->nombre ?></p>
     </div>
</div>
</div>

