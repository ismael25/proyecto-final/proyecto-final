<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
$imagen = "@web/imagenes/app-store.png"; 
?>
<div class="datos row ">
<div>
    <h3><?= $model->nombre ?></h3>
    <div class="col-md-2">
    </div>
    <div class="col-md-4">
    <?=
          Html::img($imagen, ['alt' => 'casa' , 'class' => 'image2']);
    ?>
    </div>
    <div class="col-md-4">
    <h5><b>Utilidad: </b></h5>
    <p><?= $model->utilidad ?></p>
    <h5><b>Descripción: </b></h5>
    <p><?= $model->descripcion ?></p>
    </div>
</div>
</div>