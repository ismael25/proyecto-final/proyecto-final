<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Estudiantes;

?>

<h4><?= $model->nombre ?></h4>
    <p>Telefono: <?= $model->telefono ?></p>
    <p>Email: <?= $model->correo ?></p>
    <p>DNI: <?= $model->dni ?></p>
    <p>Tiempo de estancia: <?= $model->tiempoEstancia ?> meses</p>
    <p>Estudios: <?= $model->estudios ?></p>
    <p><b>-------------------------------------------------------------------------------------------------------------------------------------------</b></p>
<h4><?= $model->codDestino->nombre ?></h4>
    <p>Coste Gestión: <?= $model->codDestino->coste ?></p>
    <p>Idiomas: </p>
    <?= implode('</p><p>', ArrayHelper::getColumn($model->codDestino->idiomas, 'idiomas')) ?>
    <p><b>-------------------------------------------------------------------------------------------------------------------------------------------</b></p>
<h4><?= $model->codEmpresa->nombre ?></h4>
    <p>Categoria: <?= $model->codEmpresa->categoría ?></p>
    <p>Trabajadores: <?= $model->codEmpresa->num_trabajadores ?></p>
    <p>Ciudad: <?= $model->codEmpresa->ciudad ?></p>
    <p>Dirección: <?= $model->codEmpresa->dirección ?></p>
    <p><b>-------------------------------------------------------------------------------------------------------------------------------------------</b></p>
    <h4>Vivienda</h4>
    <p>Direccion: <?= $model->codPiso->dirección ?></p>
    <p>Tipo: <?= $model->codPiso->tipo ?></p>
    <p>Ciudad: <?= $model->codPiso->ciudad ?></p>
    <p>Servicios: <?= $model->codPiso->servicios ?></p>
    <p>Coste Mensual:<?= $model->codPiso->coste ?></p>
    <p><b>-------------------------------------------------------------------------------------------------------------------------------------------</b></p>
    <?php $precio= ($model->codDestino->coste)+( $model->tiempoEstancia * $model->codPiso->coste  );?>
    <div><p class="text-left"><b>Presupuesto Final:</b> <?= $precio ?> €</p></div>
</div>

