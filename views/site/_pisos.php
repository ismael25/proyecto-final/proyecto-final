<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
if($model->tipo == 'casa'){
        $imagen= "@web/imagenes/casa.png"; 
    }else {
        $imagen= "@web/imagenes/departamento.png"; 
    }
?>
<div class="datos row ">
<div>
     <div class="col-md-6">
    <?=
          Html::img($imagen, ['alt' => 'casa' , 'class' => 'image2']);
    ?>
    <h5><b>Ciudad:</b></h5>
    <p> <?= $model->ciudad ?></p>
    <h5><b>Dirección:</b></h5>
    <p> <?= $model->dirección ?></p>
     </div>
     <div class="col-md-6">
    <h5><b>Pais: </b></h5>
    <p><?= $model->codDestino->nombre ?></p>
    <h5><b>Tipo: </b></h5>
    <p><?= $model->tipo ?></p>
    <h5><b>Servicios: </b></h5>
    <p><?= $model->servicios ?></p>
    <h5><b>Coste mensual: </b></h5>
    <p><?= $model->coste ?></p>
    </div>     
</div>
</div>