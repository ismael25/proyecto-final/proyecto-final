<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Apps;
use app\models\Descargan;
use app\models\Destinos;
use app\models\Documentos;
use app\models\Empresas;
use app\models\Estudiantes;
use app\models\Idiomas;
use app\models\LTuristicos;
use app\models\Necesitan;
use app\models\Pisos;
use kartik\mpdf\Pdf;







class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionDocumentos(){


        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT cod , nombre , utilidad , lugar_obtencion  FROM Documentos ',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cod' , 'nombre' , 'utilidad', 'lugar_obtencion'],
            "titulo"=>"Documentos",
        ]);
    }
    public function actionApps(){


        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT cod , nombre , utilidad , descripcion  FROM Apps ',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cod' , 'nombre' , 'utilidad' , 'descripcion'],
            "titulo"=>"Apps",
        ]);
    }
    public function actionDestinos(){


        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT cod , nombre , bandera , coste  FROM Destinos ',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cod' , 'nombre' , 'bandera' , 'coste'],
            "titulo"=>"Destinos",
        ]);
    }
    public function actionPisos(){


        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT cod , dirección , servicios , tipo , ciudad , coste  FROM Pisos ',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cod' , 'dirección' ,'servicios' , 'tipo' , 'ciudad' , 'coste'],
            "titulo"=>"Viviendas",
        ]);
    }
    public function actionEmpresas(){


        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT cod , nombre , dirección , categoría , ciudad , num_trabajadores  FROM Empresas ',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cod' , 'nombre' , 'dirección' , 'categoría' , 'ciudad' , 'num_trabajadores'],
            "titulo"=>"Empresas",
        ]);
    }
    public function actionEstudiantes(){


        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT cod , nombre , estudios , telefono , dni , tiempoEstancia , correo   FROM Estudiantes ',
            'pagination'=> [
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['cod' , 'nombre' , 'estudios' , 'telefono' , 'dni' , 'tiempoEstancia' , 'correo'],
            "titulo"=>"Empresas",
        ]);
    }

    public function actionMdestinos() 
    {
        $dataProvider = new ActiveDataProvider([

            'query' => Destinos::find(),
        ]);
        
        return $this-> render("destinos",[

            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionMempresas() 
    {
        $dataProvider = new ActiveDataProvider([

            'query' => Empresas::find(),
        ]);
        
        return $this-> render("empresas",[

            'dataProvider' => $dataProvider,
        ]);
    }
     public function actionMdocumentos() 
    {
        $dataProvider = new ActiveDataProvider([

            'query' => Documentos::find(),
        ]);
        
        return $this-> render("documentos",[

            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionMpisos() 
    {
        $dataProvider = new ActiveDataProvider([

            'query' => Pisos::find(),
        ]);
        
        return $this-> render("pisos",[

            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionMapps() 
    {
        $dataProvider = new ActiveDataProvider([

            'query' => Apps::find(),
        ]);
        
        return $this-> render("apps",[

            'dataProvider' => $dataProvider,
        ]);
    }
     public function actionMlturisticos($id) 
    {
        $dataProvider = new ActiveDataProvider([

            'query' => LTuristicos::find()
            ->where("cod_destino=$id")
        ]);
        
        return $this-> render("ltruisticos",[

            'dataProvider' => $dataProvider,
        ]);
    }
        public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([

            'query' => Estudiantes::find()
                ->where("cod=$id")
        ]);
       
     $content = $this->renderPartial('presupuestos',[

            'dataProvider' => $dataProvider,
        ]);
    
    $pdf = new Pdf([
      
        'mode' => Pdf::MODE_CORE, 
        'format' => Pdf::FORMAT_A4, 
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        'destination' => Pdf::DEST_BROWSER, 
        'content' => $content,  
        'cssFile' => 'css/site.css',
        'cssInline' => '.kv-heading-1{font-size:18px}', 
        'options' => ['title' => 'Krajee Report Title'],
        'methods' => [ 
            'SetHeader'=>['ErasmusEasy'], 
            'SetFooter'=>['{PAGENO}'],
        ]
    ]);    
    return $pdf->render(); 
    }

}
