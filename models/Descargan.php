<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "descargan".
 *
 * @property int $cod
 * @property int|null $cod_estudiantes
 * @property int|null $cod_apps
 *
 * @property Apps $codApps
 * @property Estudiantes $codEstudiantes
 */
class Descargan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'descargan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_estudiantes', 'cod_apps'], 'integer'],
            [['cod_apps'], 'exist', 'skipOnError' => true, 'targetClass' => Apps::className(), 'targetAttribute' => ['cod_apps' => 'cod']],
            [['cod_estudiantes'], 'exist', 'skipOnError' => true, 'targetClass' => Estudiantes::className(), 'targetAttribute' => ['cod_estudiantes' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'cod_estudiantes' => 'Cod Estudiantes',
            'cod_apps' => 'Cod Apps',
        ];
    }

    /**
     * Gets query for [[CodApps]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodApps()
    {
        return $this->hasOne(Apps::className(), ['cod' => 'cod_apps']);
    }

    /**
     * Gets query for [[CodEstudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstudiantes()
    {
        return $this->hasOne(Estudiantes::className(), ['cod' => 'cod_estudiantes']);
    }
}
