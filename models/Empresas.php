<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresas".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $dirección
 * @property string|null $categoría
 * @property string|null $ciudad
 * @property int|null $num_trabajadores
 * @property int|null $cod_destino
 *
 * @property Destinos $codDestino
 * @property Estudiantes[] $estudiantes
 */
class Empresas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_trabajadores', 'cod_destino'], 'integer'],
            [['nombre', 'categoría', 'ciudad'], 'string', 'max' => 50],
            [['dirección'], 'string', 'max' => 500],
            [['cod_destino'], 'exist', 'skipOnError' => true, 'targetClass' => Destinos::className(), 'targetAttribute' => ['cod_destino' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'dirección' => 'Dirección',
            'categoría' => 'Categoría',
            'ciudad' => 'Ciudad',
            'num_trabajadores' => 'Num Trabajadores',
            'cod_destino' => 'Cod Destino',
        ];
    }

    /**
     * Gets query for [[CodDestino]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDestino()
    {
        return $this->hasOne(Destinos::className(), ['cod' => 'cod_destino']);
    }

    /**
     * Gets query for [[Estudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiantes()
    {
        return $this->hasMany(Estudiantes::className(), ['cod_empresa' => 'cod']);
    }
}
