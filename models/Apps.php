<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apps".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $utilidad
 * @property string|null $descripcion
 *
 * @property Descargan[] $descargans
 */
class Apps extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apps';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'utilidad', 'descripcion'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'utilidad' => 'Utilidad',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Descargans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDescargans()
    {
        return $this->hasMany(Descargan::className(), ['cod_apps' => 'cod']);
    }
}
