<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "idiomas".
 *
 * @property int $cod
 * @property int|null $cod_destino
 * @property string|null $idiomas
 *
 * @property Destinos $codDestino
 */
class Idiomas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'idiomas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_destino'], 'integer'],
            [['idiomas'], 'string', 'max' => 50],
            [['cod_destino'], 'exist', 'skipOnError' => true, 'targetClass' => Destinos::className(), 'targetAttribute' => ['cod_destino' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'cod_destino' => 'Cod Destino',
            'idiomas' => 'Idiomas',
        ];
    }

    /**
     * Gets query for [[CodDestino]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDestino()
    {
        return $this->hasOne(Destinos::className(), ['cod' => 'cod_destino']);
    }
}
