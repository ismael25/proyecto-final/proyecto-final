<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "l_turisticos".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $dirección
 * @property string|null $ciudad
 * @property string|null $descripción
 * @property int|null $cod_destino
 * @property int|null $coste
 *
 * @property Destinos $codDestino
 */
class LTuristicos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_turisticos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_destino', 'coste'], 'integer'],
            [['nombre', 'ciudad'], 'string', 'max' => 50],
            [['dirección'], 'string', 'max' => 500],
            [['descripción'], 'string', 'max' => 200],
            [['cod_destino'], 'exist', 'skipOnError' => true, 'targetClass' => Destinos::className(), 'targetAttribute' => ['cod_destino' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'dirección' => 'Dirección',
            'ciudad' => 'Ciudad',
            'descripción' => 'Descripción',
            'cod_destino' => 'Cod Destino',
            'coste' => 'Coste',
        ];
    }

    /**
     * Gets query for [[CodDestino]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDestino()
    {
        return $this->hasOne(Destinos::className(), ['cod' => 'cod_destino']);
    }
}
