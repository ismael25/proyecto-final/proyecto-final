<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pisos".
 *
 * @property int $cod
 * @property string|null $dirección
 * @property string|null $servicios
 * @property string|null $tipo
 * @property string|null $ciudad
 * @property int|null $coste
 * @property int|null $cod_destino
 *
 * @property Estudiantes[] $estudiantes
 * @property Destinos $codDestino
 */
class Pisos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pisos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coste', 'cod_destino'], 'integer'],
            [['dirección'], 'string', 'max' => 500],
            [['servicios'], 'string', 'max' => 200],
            [['tipo', 'ciudad'], 'string', 'max' => 50],
            [['cod_destino'], 'exist', 'skipOnError' => true, 'targetClass' => Destinos::className(), 'targetAttribute' => ['cod_destino' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'dirección' => 'Dirección',
            'servicios' => 'Servicios',
            'tipo' => 'Tipo',
            'ciudad' => 'Ciudad',
            'coste' => 'Coste',
            'cod_destino' => 'Cod Destino',
        ];
    }

    /**
     * Gets query for [[Estudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiantes()
    {
        return $this->hasMany(Estudiantes::className(), ['cod_piso' => 'cod']);
    }

    /**
     * Gets query for [[CodDestino]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDestino()
    {
        return $this->hasOne(Destinos::className(), ['cod' => 'cod_destino']);
    }
}
