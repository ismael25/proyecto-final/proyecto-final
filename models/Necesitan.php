<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "necesitan".
 *
 * @property int $cod
 * @property int|null $cod_estudiantes
 * @property int|null $cod_documentos
 *
 * @property Documentos $codDocumentos
 * @property Estudiantes $codEstudiantes
 */
class Necesitan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'necesitan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_estudiantes', 'cod_documentos'], 'integer'],
            [['cod_documentos'], 'exist', 'skipOnError' => true, 'targetClass' => Documentos::className(), 'targetAttribute' => ['cod_documentos' => 'cod']],
            [['cod_estudiantes'], 'exist', 'skipOnError' => true, 'targetClass' => Estudiantes::className(), 'targetAttribute' => ['cod_estudiantes' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'cod_estudiantes' => 'Cod Estudiantes',
            'cod_documentos' => 'Cod Documentos',
        ];
    }

    /**
     * Gets query for [[CodDocumentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDocumentos()
    {
        return $this->hasOne(Documentos::className(), ['cod' => 'cod_documentos']);
    }

    /**
     * Gets query for [[CodEstudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEstudiantes()
    {
        return $this->hasOne(Estudiantes::className(), ['cod' => 'cod_estudiantes']);
    }
}
