<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documentos".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $utilidad
 * @property string|null $lugar_obtencion
 *
 * @property Necesitan[] $necesitans
 */
class Documentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'utilidad'], 'string', 'max' => 50],
            [['lugar_obtencion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'utilidad' => 'Utilidad',
            'lugar_obtencion' => 'Lugar Obtencion',
        ];
    }

    /**
     * Gets query for [[Necesitans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNecesitans()
    {
        return $this->hasMany(Necesitan::className(), ['cod_documentos' => 'cod']);
    }
}
