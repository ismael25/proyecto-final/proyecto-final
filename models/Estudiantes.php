<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudiantes".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $estudios
 * @property string|null $telefono
 * @property string|null $dni
 * @property int|null $tiempoEstancia
 * @property string|null $correo
 * @property int|null $cod_destino
 * @property int|null $cod_piso
 * @property int|null $cod_empresa
 *
 * @property Descargan[] $descargans
 * @property Destinos $codDestino
 * @property Empresas $codEmpresa
 * @property Pisos $codPiso
 * @property Necesitan[] $necesitans
 */
class Estudiantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudiantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tiempoEstancia', 'cod_destino', 'cod_piso', 'cod_empresa'], 'integer'],
            [['nombre', 'estudios', 'correo'], 'string', 'max' => 50],
            [['telefono'], 'string', 'max' => 12],
            [['dni'], 'string', 'max' => 9],
            [['cod_destino'], 'exist', 'skipOnError' => true, 'targetClass' => Destinos::className(), 'targetAttribute' => ['cod_destino' => 'cod']],
            [['cod_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresas::className(), 'targetAttribute' => ['cod_empresa' => 'cod']],
            [['cod_piso'], 'exist', 'skipOnError' => true, 'targetClass' => Pisos::className(), 'targetAttribute' => ['cod_piso' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'estudios' => 'Estudios',
            'telefono' => 'Telefono',
            'dni' => 'Dni',
            'tiempoEstancia' => 'Tiempo Estancia',
            'correo' => 'Correo',
            'cod_destino' => 'Cod Destino',
            'cod_piso' => 'Cod Piso',
            'cod_empresa' => 'Cod Empresa',
        ];
    }

    /**
     * Gets query for [[Descargans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDescargans()
    {
        return $this->hasMany(Descargan::className(), ['cod_estudiantes' => 'cod']);
    }

    /**
     * Gets query for [[CodDestino]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDestino()
    {
        return $this->hasOne(Destinos::className(), ['cod' => 'cod_destino']);
    }

    /**
     * Gets query for [[CodEmpresa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEmpresa()
    {
        return $this->hasOne(Empresas::className(), ['cod' => 'cod_empresa']);
    }

    /**
     * Gets query for [[CodPiso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPiso()
    {
        return $this->hasOne(Pisos::className(), ['cod' => 'cod_piso']);
    }

    /**
     * Gets query for [[Necesitans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNecesitans()
    {
        return $this->hasMany(Necesitan::className(), ['cod_estudiantes' => 'cod']);
    }
}
