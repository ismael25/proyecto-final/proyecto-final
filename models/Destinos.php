<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "destinos".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $bandera
 * @property int|null $coste
 *
 * @property Empresas[] $empresas
 * @property Estudiantes[] $estudiantes
 * @property Idiomas[] $idiomas
 * @property LTuristicos[] $lTuristicos
 * @property Pisos[] $pisos
 */
class Destinos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'destinos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coste'], 'integer'],
            [['nombre', 'bandera'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'bandera' => 'Bandera',
            'coste' => 'Coste',
        ];
    }

    /**
     * Gets query for [[Empresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresas()
    {
        return $this->hasMany(Empresas::className(), ['cod_destino' => 'cod']);
    }

    /**
     * Gets query for [[Estudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiantes()
    {
        return $this->hasMany(Estudiantes::className(), ['cod_destino' => 'cod']);
    }

    /**
     * Gets query for [[Idiomas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdiomas()
    {
        return $this->hasMany(Idiomas::className(), ['cod_destino' => 'cod']);
    }

    /**
     * Gets query for [[LTuristicos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLTuristicos()
    {
        return $this->hasMany(LTuristicos::className(), ['cod_destino' => 'cod']);
    }

    /**
     * Gets query for [[Pisos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPisos()
    {
        return $this->hasMany(Pisos::className(), ['cod_destino' => 'cod']);
    }
}
